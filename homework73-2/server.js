const express = require('express');
const app = express();
const port = 8000;
const Vigenere = require('caesar-salad').Vigenere;

const password = 'password';

app.get('/encode/:name', (req, res) => {
    const result = Vigenere.Cipher(password).crypt(req.params.name);

    res.send(result);
});

app.get('/decode/:hash', (req, res) => {
    const result = Vigenere.Decipher(password).crypt(req.params.hash);
    res.send(result);
});

app.listen(port);